<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class AdminController extends Controller
{
    function login() {
      return view('admin/login');
    }

    function logout(Request $request) {
        $request->session()->flush();

        return redirect('/admin');
    }

    function logincheck(Request $request) {

    	 $validatedData = $this->validate($request, [
            'email' => 'required|string|max:255|email',
            'password' => 'required|string|min:6',
	    ]);

    	$email = $request->email;
    	$password = $request->password;
		if($email != "admin@ymail.com" || $password!='111111'){
			return redirect()->back()->with('message', 'Invalid email or password');
		}

    	$file = 'users.json';
        $destinationPath=public_path()."/upload/json/"; 

    	$jsonString = file_get_contents(($destinationPath.$file));
		$old_users = json_decode($jsonString, true); 

		$request->session()->put('email', $email);
		$request->session()->put('admin_login', '1');


        return redirect('admin/home');
    }

    function home(Request $request) {
    	$file = 'users.json';
        $destinationPath=public_path()."/upload/json/"; 
    	$jsonString = file_get_contents(($destinationPath.$file));
		$users = json_decode($jsonString, true);
		unset($users['admin@ymail.com']);

        $file2 = 'candidate.json';
        $destinationPath2=public_path()."/upload/json/"; 
        $jsonString = file_get_contents(($destinationPath2.$file2));
        $candidate = json_decode($jsonString, true);

      	return view('admin/home', compact("users", "candidate"));
    }


    function add($id) {
    	$file2 = 'candidate.json';
        $destinationPath2=public_path()."/upload/json/"; 
        if(file_exists($destinationPath2.$file2)){
        	$jsonString = file_get_contents(($destinationPath2.$file2));
			$old_candidate = json_decode($jsonString, true); 
        }

		$file = 'users.json';
        $destinationPath=public_path()."/upload/json/"; 
    	$jsonString = file_get_contents(($destinationPath.$file));
		$old_users = json_decode($jsonString, true); 

		foreach ($old_users as $email => $user) {
			if($user['id'] == $id){
				$user_email = $email;
				break;
			}
		}
		$user_row = $old_users[$user_email];

		if(!empty($old_candidate[$id])){
			return redirect()->back()->with('message', 'User already added');
		}


        $old_candidate[$id] = [		
								'id' => $id,	
								'email' => $user_row['email'],
								'votes' => 0,	
                        	];

        $cand_data = json_encode($old_candidate);                        	
        try {
                
            File::put($destinationPath2.$file2,$cand_data);
            return redirect()->back()->with('message', 'User is Added');
 
        } catch(Exception $e) {
 
            return ['error' => true, 'message' => $e->getMessage()];
 
        }
    }


    function voting() {
    	$file = 'candidate.json';
        $destinationPath=public_path()."/upload/json/"; 
    	$jsonString = file_get_contents(($destinationPath.$file));
		$candidate = json_decode($jsonString, true);

      	return view('admin/voting', compact("candidate"));
    }

}
