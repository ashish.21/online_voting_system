<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $file = 'candidate.json';
        $destinationPath=public_path()."/upload/json/"; 
        $jsonString = file_get_contents(($destinationPath.$file));
        $candidate = json_decode($jsonString, true); 

        $file3 = 'voted.json';
        $destinationPath3=public_path()."/upload/json/";
        $is_voted = false;
        if(file_exists($destinationPath3.$file3)){
            $jsonString = file_get_contents(($destinationPath3.$file3));
            $old_voted = json_decode($jsonString, true);
            $id = $request->session()->get('id');  
            $is_voted = (isset($old_voted[$id])?$old_voted[$id]:'');
        }

        return view('home', compact("candidate","is_voted"));
    }


    public function vote(Request $request)
    {
        $candidate_id = $request->candidate_id;
        $file2 = 'candidate.json';
        $destinationPath2=public_path()."/upload/json/"; 
        $jsonString = file_get_contents(($destinationPath2.$file2));
        $candidate_data = json_decode($jsonString, true);
        $candidate_data[$candidate_id]['votes'] = $candidate_data[$candidate_id]['votes']+1;
        $candidate_data = json_encode($candidate_data); 

        $file3 = 'voted.json';
        $destinationPath3=public_path()."/upload/json/";
        if(file_exists($destinationPath3.$file3)){
            $jsonString = file_get_contents(($destinationPath3.$file3));
            $old_voted = json_decode($jsonString, true); 
        }

        $file = 'users.json';
        $destinationPath=public_path()."/upload/json/"; 
        $jsonString = file_get_contents(($destinationPath.$file));
        $old_users = json_decode($jsonString, true); 

        $login_email = $request->session()->get('email'); 
        $user_row = $old_users[$login_email];


        $new_voted[$user_row['id']] = [     
                                'id' => $user_row['id'],    
                                'email' => $user_row['email'],
                            ];

        $new_voted = json_encode($new_voted); 

        try {
            File::put($destinationPath2.$file2,$candidate_data);    
            File::put($destinationPath3.$file3,$new_voted);
            return redirect()->back()->with('message', 'Thanks for voting');
 
        } catch(Exception $e) {
 
            return ['error' => true, 'message' => $e->getMessage()];
 
        }
    }


    public function profileupdate(Request $request){
 
        try {
                if ($request->hasFile('profile')) {
                    $image = $request->file('profile');
                    $name = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/images');
                    $image->move($destinationPath, $name);

                    $email = $request->session()->get('email');
                    $file2 = 'users.json';
                    $destinationPath2=public_path()."/upload/json/"; 
                    $jsonString = file_get_contents(($destinationPath2.$file2));
                    $users = json_decode($jsonString, true);
                    $users[$email]['profile'] = $name;
                    $users = json_encode($users); 
                    File::put($destinationPath2.$file2,$users);
                    $request->session()->put('profile', $name);
                    return redirect()->back()->with('message', 'Image Upload successfully');
                }

 
        } catch(Exception $e) {
 
            return ['error' => true, 'message' => $e->getMessage()];
 
        }
         
        
    }

 }
