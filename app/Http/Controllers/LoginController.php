<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use File;

class LoginController extends Controller
{
    function register() {
      return view('register');
    }
 


    function registersave(Request $request) {

    	 $validatedData = $this->validate($request, [
            'email' => 'required|string|max:255|email',
            'password' => 'required|string|min:6|confirmed',
	    ]);

    	$file = 'users.json';
        $destinationPath=public_path()."/upload/json/"; 

    	$jsonString = file_get_contents(($destinationPath.$file));
		$old_users = json_decode($jsonString, true); 
		$next_id = sizeof($old_users)+1;

		$email = $request->email;
		if(!empty($old_users[$email])){
			return redirect()->back()->with('message', 'Email already exist');
		}


        $old_users[$email] = [		'id' => $next_id,
        							'email' => $request->email,
                                    'password' => bcrypt($request->password),
                                    'profile' => 'default.png'
                                	];

        $userdata = json_encode($old_users);                        	
        try {
                if (!is_dir($destinationPath)) {  
                	mkdir($destinationPath,0777,true);  
                }
                File::put($destinationPath.$file,$userdata);
                // return response()->download($destinationPath.$file);
                // return ['success' => true, 'message' => "Your registration is successful"];
                return redirect()->back()->with('message', 'Your registration is successful');

 
        } catch(Exception $e) {
 
            return ['error' => true, 'message' => $e->getMessage()];
 
        }
    }



    function login() {
      return view('login');
    }

    function logout(Request $request) {
        $request->session()->flush();

        return redirect('/login');
    }
 


    function logincheck(Request $request) {

    	 $validatedData = $this->validate($request, [
            'email' => 'required|string|max:255|email',
            'password' => 'required|string|min:6',
	    ]);

    	$file = 'users.json';
        $destinationPath=public_path()."/upload/json/"; 

    	$jsonString = file_get_contents(($destinationPath.$file));
		$old_users = json_decode($jsonString, true); 

		$email = $request->email;
        
		if(empty($old_users[$email])){
			return redirect()->back()->with('message', 'Invalid email or password');
		}else{
            $id = $old_users[$email]['id'];
            $profile = $old_users[$email]['profile'];
        }

		$request->session()->put('email', $email);
        $request->session()->put('id', $id);
		$request->session()->put('login', '1');
        $request->session()->put('profile', $profile);


        return redirect('/home');



    }

}
