@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <li><a href="{{ route('voting') }}">Candidate List</a></li>
                @if(session()->has('message'))
                    <?php 
                    $style = "";
                    if(session()->get('message')=="User already added"){
                        $style = "color:red";
                    }

                    ?>
                    <div class="alert alert-success" style="{{$style}}">
                        {{ session()->get('message') }}
                    </div>
                @endif

                <div class="panel-body">
                    
                    <table>
                        <thead>
                            <th>Sr No.</th>
                            <th>Email</th>
                            <th>Add to voting</th>
                        </thead>
                        <tbody style="line-height: 2">
                            <?php $i = 1; ?>    
                            @foreach ($users as $user)
                            <?php 
                                 $id = $user['id'];
                                 //$candidate_row = !empty($candidate[$id])?true:false;
                            ?>    
                            <tr>
                                <td style="padding-right: 10px">{{$i}}</td>
                                <td style="padding-right: 10px">{{$user['email']}}</td>
                                <td style="padding-right: 10px"><a href="add/<?= $id ?>">Add</a></td>
                            </tr>
                            <?php $i++;?>    
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
