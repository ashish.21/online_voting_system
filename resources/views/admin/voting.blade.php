@extends('layouts.app2')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <li><a href="{{ route('admin_home') }}">Home</a></li>
                @if(session()->has('message'))
                    <?php 
                    $style = "";
                    if(session()->get('message')=="User already added"){
                        $style = "color:red";
                    }

                    ?>
                    <div class="alert alert-success" style="{{$style}}">
                        {{ session()->get('message') }}
                    </div>
                @endif

                <div class="panel-body">
                    @if(isset($candidate))
                    <table>
                        <thead>
                            <th>Sr No.</th>
                            <th>Email</th>
                            <th>Votes</th>
                        </thead>
                        <tbody style="line-height: 2">
                            
                                <?php $i = 1;?>    
                                @foreach ($candidate as $candidate_row)
                                <?php 
                                     $id = isset($candidate_row['id'])?$candidate_row['id']:'';
                                ?>    
                                <tr>
                                    <td style="padding-right: 10px">{{$i}}</td>
                                    <td style="padding-right: 10px">{{$candidate_row['email']}}</td>
                                    <td style="padding-right: 10px">{{$candidate_row['votes']}}</td>
                                </tr>
                                <?php $i++;?>    
                                @endforeach
                            


                            
                        </tbody>
                    </table>
                    @else
                        <tr>
                            No candidate available
                        </tr>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
