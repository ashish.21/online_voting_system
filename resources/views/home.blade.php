@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Vote</div>

                @if(session()->has('message'))
                    <?php 
                    $style = "";
 
                    ?>
                    <div class="alert alert-success" style="{{$style}}">
                        {{ session()->get('message') }}
                    </div>
                @endif


                <?php 
                if(!$is_voted){
                ?>
                    <div class="panel-body">
                        @if(isset($candidate))
                             <form class="form-horizontal" method="POST" action="{{ route('submit_vote') }}">
                                {{ csrf_field() }}
                                @foreach ($candidate as $candidate_row)
                                    <?php 
                                         $id = $candidate_row['id'];
                                    ?>    
                                    <div>
                                        <input type="radio" name="candidate_id" value="{{$id}}">
                                        <span>{{$candidate_row['email']}}</span>
                                    </div>
                                 @endforeach

                                  <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Vote
                                            </button>
                                        </div>
                                    </div>
                                </form>
                        @else
                            <tr>No candidate available</tr>
                        @endif
                       

                    </div>
                <?php 
                }else{
                ?>    
                    <div class="panel-body">
                        Thanks for voting
                    </div>
                <?php }
                ?>
                
            </div>
        </div>
    </div>
</div>
@endsection
