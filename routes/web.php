<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/register', 'LoginController@register')->name('register');
Route::post('/register', 'LoginController@registersave');

Route::get('/login', 'LoginController@login')->name('login');
Route::post('/login', 'LoginController@logincheck');
Route::get('/logout', 'LoginController@logout')->name('logout');

Route::get('/home', ['middleware' => 'authen', 'uses' => 'HomeController@index'])->name('home');
Route::post('/home', ['middleware' => 'authen', 'uses' => 'HomeController@profileupdate']);
Route::post('/home/vote', ['middleware' => 'authen', 'uses' => 'HomeController@vote'])->name('submit_vote');

Route::get('/admin', 'AdminController@login')->name('admin');
Route::post('/admin', 'AdminController@logincheck');
Route::get('/admin/logout', 'AdminController@logout')->name('admin_logout');
Route::get('/admin/home', ['middleware' => 'admin', 'uses' => 'AdminController@home'])->name('admin_home');
Route::get('/admin/add/{email}', ['middleware' => 'admin', 'uses' => 'AdminController@add']);
Route::get('/admin/voting', ['middleware' => 'admin', 'uses' => 'AdminController@voting'])->name('voting');

